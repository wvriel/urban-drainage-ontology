---
title: The Urban Drainage Ontology - GWSW
---
<!--: .wrap .size-40 ..aligncenter bgimage=images/toilet_visit.jpg -->

<!-- : .flexblock features -->
- <div>
<h1>**The Urban Drainage Ontology**</h1>
<h2>An implementation in an asset management setting</h2>
<br>
<p>[**Wouter van Riel, PhD**](https://www.linkedin.com/in/wouter-van-riel/)</p>
<p>**SEMANTiCS conference, Amsterdam, 8 September 2021**</p>
</div>

<!-- : .flexblock clients -->
- <div>![logo rioned foundation](images/logo rioned.png)</div>
- <div>![logo infralytics](images/logo_infralytics.png)</div>

---
<!-- : .wrap .size-70 ..aligncenter bgimage=images/background_sewers.png -->

## **public space & an underground pipe network**

<!-- : .flexblock features -->
- <div><h2>{{< svg fa-bar-chart >}} 200,000 km of pipes</h2>Main & secondary</div>
- <div><h2>{{< svg fa-eur >}} 1.6 billion Euro per year</h2>Asset management expenses</div>
- <div><h2>{{< svg fa-users >}} 352 municipalities</h2>17.5 million inhabitants</div>

---
<!--: .wrap .size-80 .fadeInUp bgimage=images/timeline.png -->

# development timeline

<!-- : .flexblock features -->
- <div><h2>1990s</h2>first edition of dictionary</div>
- <div><h2>2004-2009</h2>first attempt to create semantic model; result not accepted by market</div>
- <div><h2>2013-2014</h2>broad commitment and rebuilt of semantic model</div>
- <div><h2>2018</h2>first operational application</div>
- <div><h2>today</h2>data sharing nation-wide, various applications</div>
- <div><h2>tomorrow</h2>integration into public space ontology</div>

___
<!--:  bg=bg-gradient-h ..aligncenter -->

## **why the UDO?**

<!-- : .text-intro -->Municipalities had the need to uniform terms
<br>
<!-- : .text-intro -->Innovations require information models and uniform data

![problem](images/problem2.png)


---
<!--: .wrap ..aligncenter bg=bg-green -->
# **ontology content**
<br>

<!-- : .text-intro -->OWL & RDFS based

<!--: .flexblock features  -->
- {{< flexblock "<span>4600</span>Concepts" >}}Grouped in various modules{{< /flexblock >}}
- <div><h2>{{< svg fa-building >}}Physical objects</h2>Pipes, manholes, pumps, etc</div>
- <div><h2>{{< svg fa-check-square >}}Activities</h2>Replacement, inspections, repairs</div>
- <div><h2>{{< svg fa-list >}}Attributes</h2>Length, materials, start date</div>
- <div><h2>{{< svg fa-file >}}Data exchange formats</h2>**Primary:** RDF, Turtle. **Secondary:** Conversion to formats for GIS, 3D, hydraulic modelling, and inspection</div>
- <div><h2>{{< svg fa-code >}}Relations</h2>Physical parts, network connections</div>

<!-- : .text-intro -->The entire ontology is setup by experts from the urban drainage sector

<!-- : .text-intro -->Modular setup: development through user groups

---
<!--: .wrap bg=bg-purple -->

|||
# **the big leap**

<!-- : .text-intro -->from a theoretical model to implementation

![giant leap](images/big_leap.png)

|||v

<!--: .flexblock features  -->
- <div><h2>Software implementation</h2></div>
- <div><h2>Commitment & awareness creation</h2></div>
- <div><h2>Human friendly portal</h2>
<a href="https://data.gwsw.nl">ontology portal</a> and 
<a href="https://apps.gwsw.nl">application portal</a></div>
- <div><h2>Applications</h2>Data validation, modelling files, GIS visualisation, filters</div>
- <div><h2>Documents</h2>

---
<!--: .wrap bg=bg-purple -->

|||
# **the big leap** {{< svg fa-check-square >}}

<!-- : .text-intro -->from a theoretical model to implementation

![giant leap](images/big_leap.png)

|||v

<!--: .flexblock features  -->
- <div><h2>&check; Software implementation</h2></div>
- <div><h2>&check; Commitment & awareness creation</h2></div>
- <div><h2>&check; Human friendly portal</h2>
<a href="https://data.gwsw.nl">ontology portal</a> and 
<a href="https://apps.gwsw.nl">application portal</a>
</div>
- <div><h2>&check; Applications</h2>Data validation, modelling files, GIS visualisation, filters by use case</div>
- <div><h2>&check; Documents</h2>

---

<!--: .wrap bg=bg-secondary ..aligncenter -->

## example: public governmental data portal

<div>
 <iframe style="border: 0; width:70%; height:600px; overflow: auto" src="https://www.pdok.nl/viewer/">
 </iframe>
</div>

---
<!--: .wrap bg=bg-blue ..aligncenter -->

## example: some large municipality in NL

<br>

<!-- : .text-intro -->All contractors' activities into ontology, linked data sharing

<br>

![contractors](images/contractor.png)

---
<!--: .wrap ..align-left bg=bg-apple bgimage=images/pipe_burst.jpg -->


<!-- : .flexblock features -->
- <div>
<h1>example: pressurised <br> pipe incidents</h1>
<h2>uniform registration format for better analysis</h2>
</div>


---
<!-- : bg=bg-apple .aligncenter -->

## thank you for listening
## any questions?
<br><br>
<a href="mailto:wouter.van.riel@infralytics.org">wouter.van.riel@infralytics.org</a>
<br><br><br>
Ontology: [https://data.gwsw.nl](https://data.gwsw.nl)
<br>
Data and applications: [https://apps.gwsw.nl](https://apps.gwsw.nl)
<br>
SPARQL end point: [https://sparql.gwsw.nl](https://sparql.gwsw.nl)
